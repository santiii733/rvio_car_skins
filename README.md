# rvio_car_skins
Additional skins for cars included in the [rvio_cars](https://gitlab.com/URV/rvio_cars) package.

Latest [stable version](https://gitlab.com/URV/rvio_car_skins/tree/stable): **18.0421**<br>
Official download available [here](https://re-volt.io/online/packs).
